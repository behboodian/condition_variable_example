/* 
 * File:   condition_variable.h
 * Author: ali
 *
 * Created on April 6, 2014, 5:12 PM
 */

#ifndef CONDITION_VARIABLE_H
#define	CONDITION_VARIABLE_H

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define TCOUNT 10
#define COUNT_LIMIT 12
#define NUM_THREADS  3

int count;
int thread_ids[3];
pthread_mutex_t count_mutex;
pthread_cond_t count_threshold_cv;

void *inc_count(void *t);
void *watch_count(void *t);

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* CONDITION_VARIABLE_H */

